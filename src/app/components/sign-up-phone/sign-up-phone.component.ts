import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-sign-up-phone',
  templateUrl: './sign-up-phone.component.html',
  styleUrls: ['./sign-up-phone.component.css']
})
export class SignUpPhoneComponent implements OnInit {
 @Output() code = new EventEmitter<string>()
  constructor() { }

  ngOnInit(): void {
  }
  sendCode(code:string){
    this.code.emit(code)
  }
}
