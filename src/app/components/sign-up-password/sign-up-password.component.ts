import {AfterViewInit, Component, OnInit} from '@angular/core';
import {gsap} from "gsap";

@Component({
  selector: 'app-sign-up-password',
  templateUrl: './sign-up-password.component.html',
  styleUrls: ['./sign-up-password.component.css']
})
export class SignUpPasswordComponent implements OnInit, AfterViewInit {

  constructor() {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    gsap.from('.sign__up_password_animate',{
      opacity:0,
      duration:1,
      y:50,
    })
  }
}
