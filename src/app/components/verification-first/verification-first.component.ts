import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-verification-first',
  templateUrl: './verification-first.component.html',
  styleUrls: ['./verification-first.component.css']
})
export class VerificationFirstComponent implements OnInit {
  @Output() firstToSecond = new EventEmitter<string>()

  constructor() {
  }

  ngOnInit(): void {
  }

  verifySecond(verificationPage: string) {
    this.firstToSecond.emit(verificationPage)
  }
}
