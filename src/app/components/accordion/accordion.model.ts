export class AccordionModel {
  title: string = ''
  parentLink: string = ''
  menu: boolean = false
  active?: boolean
  submenu!: { childText: string; link: string, active: boolean }[];
}
