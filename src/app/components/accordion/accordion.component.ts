import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AccordionModel} from "./accordion.model";


@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss']
})
export class AccordionComponent implements OnInit {
  @Output() menuLink = new EventEmitter<string>()

  result: AccordionModel[] = [
    {
      "title": "Exchange USDT",
      "parentLink": "/",
      "menu": false,
      "submenu": [
        {
          "childText": "sell",
          "link": "sell",
          "active": false
        },
        {
          "childText": "Buy",
          "link": "buy",
          "active": false
        }
      ]
    },
    {
      "title": "Addresses",
      "menu": false,
      "parentLink": "",
      "submenu": [
        {
          "childText": "USDT address",
          "link": "usdt-address",
          "active": false
        },
        {
          "childText": "Bank Card",
          "link": "bank-card",
          "active": false
        }
      ]
    },
    {
      "title": "verification",
      "menu": false,
      "parentLink": "verification",
      "active": false,
      "submenu": []
    },
    {
      "title": "Activity",
      "menu": false,
      "parentLink": "activity",
      "active": false,
      "submenu": []
    },
    {
      "title": "Сhat",
      "parentLink": "chat",
      "menu": false,
      "active": false,
      "submenu": []
    },
    {
      "title": "Safity",
      "parentLink": "/",
      "menu": false,
      "submenu": [
        {
          "childText": "Change password",
          "link": "change-password",
          "active": false
        },
        {
          "childText": "Change phone number",
          "link": "change-phone",
          "active": false
        }
      ]
    },
    {
      "title": "Log Out",
      "parentLink": "logout",
      "menu": false,
      "submenu": []
    }
  ]

  constructor() {

  }


  ngOnInit(): void {
  }

  ddToggle(item: any, i: number) {
    this.result[i].menu = !this.result[i].menu;
    this.result.forEach(item => {
      if (item.active !== undefined) {item.active = false}
      if (item.submenu) {item.submenu.forEach(sub => {sub.active = false})}
    })
    this.result[i].active = true
    if (item.parentLink !== "/") {this.collection(item.parentLink)}
  }
  passInto(parChild: string, i: number, index: number) {
    this.result.forEach(item => {
      if (item.active !== undefined) {item.active = false}
      if (item.submenu) {item.submenu.forEach(sub => {sub.active = false})}})
    this.result[i].submenu[index].active = true
    this.collection(parChild)
  }

  collection(menu: string) {
    this.menuLink.emit(menu)
  }

}

