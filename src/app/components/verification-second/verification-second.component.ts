import {AfterViewInit, Component, OnInit} from '@angular/core';
import {gsap} from "gsap";

@Component({
  selector: 'app-verification-second',
  templateUrl: './verification-second.component.html',
  styleUrls: ['./verification-second.component.css']
})
export class VerificationSecondComponent implements OnInit , AfterViewInit{
  imagePath:any = 'assets/images/default-upload.svg'
  constructor() { }

  ngOnInit(): void {
  }
  fnChange(event:any){
    let target = event.target
    let selectedFile = target.files[0]
    let type = selectedFile.type.split('/')[0]
    if (type != 'image') {
      alert('пожалуйста, выберите изображение')
      return
    }
    let fileReader = new FileReader()
    fileReader.readAsDataURL(selectedFile)
    fileReader.onload = () => {
      let result = fileReader.result
      this.imagePath = result
    }
    fileReader.onerror = () => {
      this.imagePath = 'assets/images/default-upload.svg'
    }
  }
  ngAfterViewInit() {
    gsap.from('.second__verify_animate',{
      opacity:0,
      duration:1,
      y:50
    })
  }
}
