import {AfterViewInit, Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {Router} from "@angular/router";
import {gsap} from "gsap";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/auth.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-join-us',
  templateUrl: './join-us.component.html',
  styles: []
})
export class JoinUsComponent implements OnInit, AfterViewInit, OnDestroy {
  identity: string = ''
  message: string = 'Код подтверждения был отправлен на номер: 99890******45'
  encKey: string = ''
  sub!: Subscription
  authForm: FormGroup = new FormGroup({
    username: new FormControl('998', Validators.compose([Validators.minLength(12)])),
    passwordForm: new FormGroup({
      password: new FormControl('',Validators.compose([Validators.minLength(8),Validators.maxLength(8)]))
    }),
    smsCodeForm: new FormGroup({
      code: new FormControl('')
    })
  }, {updateOn: 'change'})
  showPassword: boolean = false
  phone: boolean = true
  smsCode: boolean = false

  constructor(
    public router: Router,
    private auth: AuthService
  ) {
  }

  ngOnInit(): void {
  }

  authStep1() {
    if (this.authForm.valid) {
      this.sub = this.auth.signInCodeSend(this.authForm.value).subscribe((res) => {
        if (res.success) {
          this.identity = res.result.data.identity
          this.encKey = res.result.data.encKey
          this.phone = false
          this.showPassword = true
          gsap.from('.password__animate', {
            opacity: 0,
            duration: 1,
            y: 50
          })
        }
      })
    }
  }

  authStep2() {
    if (this.password?.valid) {
      this.sub = this.auth.signCheck(this.password.value, this.encKey, this.identity).subscribe((res) => {
        if (res.success) {
          this.message = res.result.data.message
          this.showPassword = false
          this.smsCode = true
        }
      })
    }
  }

  authStep3() {
    if (this.code?.valid) {
      this.sub = this.auth.signInConfirm({
        identity: this.identity,
        code: this.code.value
      }).subscribe((res) => {
        console.log(res)
      })
    }
  }

  ngAfterViewInit() {
    gsap.from('.animate_dashboard_join_us', {
      opacity: 0,
      duration: 1,
      y: 50
    })

  }

  get password() {
    return this.authForm.get('passwordForm.password')
  }

  get code() {
    return this.authForm.get('smsCodeForm.code')
  }

  ngOnDestroy() {
    this.sub.unsubscribe()
  }
}
