export type Carousel = {
  date: string;
  title: string;
  content: string;
};
