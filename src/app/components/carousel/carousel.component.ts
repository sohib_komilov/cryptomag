import {Component, AfterViewInit} from '@angular/core';
import {carousel} from "./carousel.constant";

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
})
export class CarouselComponent implements AfterViewInit {

  carousel = carousel

  ngAfterViewInit() {
  }

  ngOnDestroy() {
  }
}
