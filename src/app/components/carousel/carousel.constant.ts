import {Carousel} from "./carousel.types";
const today: any = new Date()
const result:string = today.getDay() + ' ' + today.toLocaleString('en-Us', {month: 'long'})+' ' +today.getFullYear()
export const carousel: Carousel[] = [
  {
    date:result,
    title: 'Lorem ipsum dolor sit amet, consectetur  elit, sed do eiusmod tempor.',
    content: 'Lorem ipsum dolor sit amet, consectetur  elit, sed do eiusmod tempor incididuna. Ut enim ad minim veniam',
  },
  {
    date:result,
    title: 'Lorem ipsum dolor sit amet, consectetur  elit, sed do eiusmod tempor.',
    content: 'Lorem ipsum dolor sit amet, consectetur  elit, sed do eiusmod tempor incididuna. Ut enim ad minim veniam\n' +
      'Lorem ipsum dolor sit amet, consectetur  elit, sed do eiusmod tempor incididuna.',
  },
  {
    date:result,
    title: 'item-3',
    content: 'Lorem ipsum dolor sit amet, consectetur  elit, sed do eiusmod tempor incididuna. Ut enim ad minim veniam',
  },
];
