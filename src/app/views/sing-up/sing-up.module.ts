import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SingUpComponent} from "./sing-up.component";
import {SingUpRoutingModule} from "./sing-up-routing.module";
import {SignUpPhoneComponent} from "../../components/sign-up-phone/sign-up-phone.component";
import {SignUpPasswordComponent} from "../../components/sign-up-password/sign-up-password.component";
import {CabinetModule} from "../cabinet/cabinet.module";
import {CarouselComponent} from "../../components/carousel/carousel.component";
import {IvyCarouselModule} from "angular-responsive-carousel";



@NgModule({
  declarations: [
    SingUpComponent,
    SignUpPhoneComponent,
    SignUpPasswordComponent,
    CarouselComponent,

  ],
  imports: [
    CommonModule,
    SingUpRoutingModule,
    CabinetModule,
    IvyCarouselModule
  ],
  exports: [SingUpComponent]
})
export class SingUpModule {
}
