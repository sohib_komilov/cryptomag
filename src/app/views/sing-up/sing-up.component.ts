import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Carousel} from "../../components/carousel/carousel.types";
import {gsap} from "gsap";
@Component({
  selector: 'app-sing-up',
  templateUrl: './sing-up.component.html',
  styleUrls: ['./sing-up.component.css']
})
export class SingUpComponent implements OnInit , AfterViewInit {
  form: string = 'phone'
  items = [{ title: 'Slide 1' }, { title: 'Slide 2' }, { title: 'Slide 3' }];


  ngOnInit() {
  }

  receiveCode(code: string) {
    code === '1234567' ? this.form = 'password' : this.form = 'phone'
  }
  carousel: Carousel[] = [
    {
      date:this.getMonthDayYear(),
      title: 'Lorem ipsum dolor sit amet, consectetur  elit, sed do eiusmod tempor.',
      content: 'Lorem ipsum dolor sit amet, consectetur  elit, sed do eiusmod tempor incididuna. Ut enim ad minim veniam',
    },
    {
      date:this.getMonthDayYear(),
      title: 'Lorem ipsum dolor sit amet, consectetur  elit, sed do eiusmod tempor.',
      content: 'Lorem ipsum dolor sit amet, consectetur  elit, sed do eiusmod tempor incididuna. Ut enim ad minim veniam\n' +
        'Lorem ipsum dolor sit amet, consectetur  elit, sed do eiusmod tempor incididuna.',
    },
    {
      date:this.getMonthDayYear(),
      title: 'Lorem ipsum dolor sit amet, consectetur  elit, sed do eiusmod tempor.',
      content: 'Lorem ipsum dolor sit amet, consectetur  elit, sed do eiusmod tempor incididuna. Ut enim ad minim veniam',
    },
  ];
  getMonthDayYear() {
    const today: any = new Date()
    const day: string = today.getDay()
    const year: string = today.getFullYear()
    const month = today.toLocaleString('en-Us', {month: 'long'})
    return month + ' ' + day + ', ' + year
  }
  ngAfterViewInit() {
    gsap.from('.sign__up_otp',{
      opacity:0,
      duration:1,
      y:50
    })
  }
}
