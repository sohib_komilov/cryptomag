import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {CabinetComponent} from "./cabinet.component";
import {SellComponent} from "../../components/accordion/sell/sell.component";
import {BuyComponent} from "../../components/accordion/buy/buy.component";
import {VerifyComponent} from "../../components/accordion/verify/verify.component";
import {ChangePasswordComponent} from "../../components/accordion/change-password/change-password.component";
import {ChangePhoneNumberComponent} from "../../components/accordion/change-phone-number/change-phone-number.component";
import {BankCardComponent} from "../../components/accordion/bank-card/bank-card.component";
import {ActivityComponent} from "../../components/accordion/activity/activity.component";
import {ChatComponent} from "../../components/accordion/chat/chat.component";
import {UsdtComponent} from "../../components/accordion/usdt/usdt.component";


const routes: Routes = [
  {
    path: '', component: CabinetComponent,
    children: [
      {path: 'sell', component: SellComponent, data: {state: 'sell'}},
      {path: 'verification', component: VerifyComponent,data: {state: 'verification'}},
      {path: 'buy', component: BuyComponent,data: {state: 'buy'}},
      {path: 'change-password', component: ChangePasswordComponent,data: {state: 'change-password'}},
      {path: 'change-phone', component: ChangePhoneNumberComponent,data: {state: 'change-phone'}},
      {path: 'activity', component: ActivityComponent,data: {state: 'activity'}},
      {path: 'chat', component: ChatComponent,data: {state: 'chat'}},
      {path: 'bank-card', component: BankCardComponent,data: {state: 'bank-card'}},
      {path: 'usdt-address', component: UsdtComponent,data: {state: 'usdt-address'}},
    ]
  },
]
@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class RoutingCabinetModule {
}
