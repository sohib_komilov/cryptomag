import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CabinetComponent} from "./cabinet.component";
import {RoutingCabinetModule} from "./routing-cabinet.module";
import {AccordionComponent} from "../../components/accordion/accordion.component";
import {SellComponent} from "../../components/accordion/sell/sell.component";
import {BuyComponent} from "../../components/accordion/buy/buy.component";
import {BankCardComponent} from "../../components/accordion/bank-card/bank-card.component";
import {UsdtComponent} from "../../components/accordion/usdt/usdt.component";
import {VerifyComponent} from "../../components/accordion/verify/verify.component";
import {ActivityComponent} from "../../components/accordion/activity/activity.component";
import {ChatComponent} from "../../components/accordion/chat/chat.component";
import {ChangePasswordComponent} from "../../components/accordion/change-password/change-password.component";
import {ChangePhoneNumberComponent} from "../../components/accordion/change-phone-number/change-phone-number.component";



@NgModule({
  declarations: [
    CabinetComponent,
    AccordionComponent,
    SellComponent,
    BuyComponent,
    BankCardComponent,
    UsdtComponent,
    VerifyComponent,
    ActivityComponent,
    ChatComponent,
    ChangePasswordComponent,
    ChangePhoneNumberComponent,

  ],
  imports: [
    CommonModule,
    RoutingCabinetModule,
  ]
})
export class CabinetModule {
}
