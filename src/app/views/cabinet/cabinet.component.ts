import {Component,  OnInit} from '@angular/core';
import {animate, group, query, style, transition, trigger} from "@angular/animations";

@Component({
  selector: 'app-cabinet',
  templateUrl: './cabinet.component.html',
  styleUrls: ['./cabinet.component.css'],
  animations: [
    trigger('routerTransition', [
      transition('* <=> *', [
        query(':enter, :leave', style({ position: 'fixed', width:'100%' }),{optional:true}),
        group([
          query(':enter', [
            style({ transform: 'translateX(100%)' }),
            animate('1s ease-in-out', style({ transform: 'translateX(0%)' }))
          ],
            {
              optional:true
            }),
          query(':leave', [
            style({ transform: 'translateX(0%)' }),
            animate('1s ease-in-out', style({ transform: 'translateX(100%)' }))]
          ,{optional: true}
          ),
        ])
      ])
    ])
  ],
})
export class CabinetComponent implements OnInit {
  activate: string = 'sell'

  constructor() {
  }
  ngOnInit(): void {
  }

  getState(outlet:any) {
    return outlet.activatedRouteData.state;
  }

  link(link: string) {
    const sub = link.substring(1, link.length)
    this.activate = sub
  }
}
