import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DashboardComponent} from "./dashboard.component";
import {JoinUsComponent} from "../../components/join-us/join-us.component";
import {DownloadButtonsComponent} from "../../components/download-buttons/download-buttons.component";
import {AboutUsComponent} from "../../components/about-us/about-us.component";
import {CalculatorComponent} from "../../components/calculator/calculator.component";
import {NewsComponent} from "../../components/news/news.component";
import {DashboardRoutingModule} from "./dashboard-routing.module";
import {IvyCarouselModule} from "angular-responsive-carousel";
import {IConfig, NgxMaskModule} from "ngx-mask";
import {ReactiveFormsModule} from "@angular/forms";

const maskConfig: Partial<IConfig> = {
  validation: false
}

@NgModule({
  declarations: [
    DashboardComponent,
    JoinUsComponent,
    DownloadButtonsComponent,
    AboutUsComponent,
    CalculatorComponent,
    NewsComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    IvyCarouselModule,
    NgxMaskModule.forRoot(maskConfig),
    ReactiveFormsModule

  ],
  exports: [DashboardComponent, DownloadButtonsComponent]
})
export class DashboardModule {
}
