import {AfterViewInit, Component, OnInit} from '@angular/core';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit ,AfterViewInit{
  urlClear:boolean = true
  dashboard_title: string = 'Lorem ipsum dolor sit'
  join_words:string = ' Lorem ipsum dolor sit amet, consectetur elit, sed do eiusmod tempor incididuna. Ut enim ad minim veniam'
  some_words: string = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididuna.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.'
  some_words_bottom: string = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.'
  missionElements: Array<any> = [
    {
      img: 'assets/images/Vector.png',
      title: 'Our Mission',
      content: 'Lorem ipsum dolor sit amet, adipiscing elit, sed do eiusmod tempor incididuna'
    },
    {
      img: 'assets/images/Vector.png',
      title: 'Our Mission',
      content: 'Lorem ipsum dolor sit amet, adipiscing elit, sed do eiusmod tempor incididuna'
    },
    {
      img: 'assets/images/Vector.png',
      title: 'Our Mission',
      content: 'Lorem ipsum dolor sit amet, adipiscing elit, sed do eiusmod tempor incididuna'
    }
  ]

  news: Array<any> = [
    {
      date: this.getMonthDayYear(),
      title: "Lorem ipsum dolor sit amet, consectetur  elit, sed do eiusmod tempor.",
      content: "Lorem ipsum dolor sit amet, consectetur  elit, sed do eiusmod tempor incididuna. Ut enim ad minim veniam"
    },
    {
      date: this.getMonthDayYear(),
      title: "Lorem ipsum dolor sit amet, consectetur  elit.",
      content: "Lorem ipsum dolor sit amet, consectetur  elit, sed do eiusmod tempor incididuna"
    },
    {
      date: this.getMonthDayYear(),
      title: "Lorem ipsum dolor sit amet, consectetur  elit, sed do eiusmod tempor.",
      content: "Lorem ipsum dolor sit amet, consectetur  elit, sed do eiusmod tempor incididuna. Ut enim ad minim veniam"
    },
    {
      date: this.getMonthDayYear(),
      title: "Lorem ipsum dolor sit amet, consectetur  elit, sed do eiusmod tempor.",
      content: "Lorem ipsum dolor sit amet, consectetur  elit, sed do eiusmod tempor incididuna. Ut enim ad minim veniam"
    },
    {
      date: this.getMonthDayYear(),
      title: "Lorem ipsum dolor sit amet, consectetur  elit, sed do eiusmod tempor.",
      content: "Lorem ipsum dolor sit amet, consectetur  elit, sed do eiusmod tempor incididuna. Ut enim ad minim veniam"
    },
    {
      date: this.getMonthDayYear(),
      title: "Lorem ipsum dolor sit amet, consectetur  elit, sed do eiusmod tempor.",
      content: "Lorem ipsum dolor sit amet, consectetur  elit, sed do eiusmod tempor incididuna. Ut enim ad minim veniam"
    },
    {
      date: this.getMonthDayYear(),
      title: "Lorem ipsum dolor sit amet, consectetur  elit, sed do eiusmod tempor.",
      content: "Lorem ipsum dolor sit amet, consectetur  elit, sed do eiusmod tempor incididuna. Ut enim ad minim veniam"
    },
  ]

  constructor() {
  }

  ngOnInit(): void {
  }
  getMonthDayYear() {
    const today: any = new Date()
    const day: string = today.getDay()
    const year: string = today.getFullYear()
    const month = today.toLocaleString('en-Us', {month: 'long'})
    return month + ' ' + day + ', ' + year
  }
  ngAfterViewInit() {

  }
}
