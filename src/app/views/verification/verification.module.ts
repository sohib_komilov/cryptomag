import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {VerificationComponent} from "./verification.component";
import {VerificationRoutingModule} from "./verification-routing.module";
import {VerificationFirstComponent} from "../../components/verification-first/verification-first.component";
import {VerificationSecondComponent} from "../../components/verification-second/verification-second.component";
import {IvyCarouselModule} from "angular-responsive-carousel";



@NgModule({
  declarations: [VerificationComponent,VerificationFirstComponent,VerificationSecondComponent],
    imports: [
        CommonModule,
        VerificationRoutingModule,
        IvyCarouselModule
    ],
  exports: [VerificationComponent]
})
export class VerificationModule { }
