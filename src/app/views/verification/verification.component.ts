import {AfterViewInit, Component, OnInit} from '@angular/core';
import {gsap} from "gsap";
import {carousel} from "../../components/carousel/carousel.constant";

@Component({
  selector: 'app-verification',
  templateUrl: './verification.component.html',
  styleUrls: ['./verification.component.css']
})
export class VerificationComponent implements OnInit,AfterViewInit {
  to: string = 'verify1'
  img: string = 'assets/images/sitting-man.svg'
  confirmOrApp: string = 'Confirm your identity'
carousel = carousel
  constructor() {
  }

  ngOnInit(): void {
  }

  first(page: string) {
    if (page === 'second'){
      this.to = 'verify2'
      this.img = 'assets/images/standing-man.svg'
      this.confirmOrApp = 'Or continue with our app '
    }
    else {
      this.to = 'verify1'
      this.img = 'assets/images/sitting-man.svg'
      this.confirmOrApp = 'Confirm your identity'
    }
  }
  ngAfterViewInit() {
    gsap.from('.verification__animate',{
      opacity:0,
      duration:1,
      y:50
    })
  }

}
