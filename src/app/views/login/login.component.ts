import {AfterViewInit, Component, OnInit} from '@angular/core';
import {gsap} from "gsap";
import {carousel} from "../../components/carousel/carousel.constant";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit , AfterViewInit {
  carousel = carousel
  constructor() {
  }

  ngOnInit(): void {
  }
  ngAfterViewInit() {
    gsap.from('.animate__top',{
      opacity:0,
      duration:1,
      y:50
    })
  }
}
