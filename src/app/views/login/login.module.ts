import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LoginComponent} from "./login.component";
import {LoginRoutingModule} from "./login-routing.module";
import {SignInComponent} from "../../components/sign-in/sign-in.component";
import {IvyCarouselModule} from "angular-responsive-carousel";



@NgModule({
  declarations: [
    LoginComponent,
    SignInComponent,
  ],
  imports: [
    CommonModule,
    LoginRoutingModule,
    IvyCarouselModule
  ],
  exports: [LoginComponent]
})
export class LoginModule { }
