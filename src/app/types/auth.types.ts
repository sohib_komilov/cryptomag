export type username = {
  username: string
};
export type signInConfirm = {
  code: string
  identity: string
};
export type signCodeResponse = {
  success: boolean
  result: signCodeResult
}
export type signCodeResult = {
  code: number;
  message: string;
  audit: string;
  data: any
};
export type cookieResponse = {
  success: boolean
  result: {
    code: number
    message: string
    audit: string
    data: {
      user: {
        username: string
        name: string
        role: string
        status: string
      }
      access: {
        token: string
        expire: number
      }
      needInfo: boolean
    } | null
  }
}
