import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';


const routes: Routes = [
  {path: 'dashboard', loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)},
  {path: 'register', loadChildren: () => import('./views/sing-up/sing-up.module').then(m => m.SingUpModule)},
  {path: 'login', loadChildren: () => import('./views/login/login.module').then(m => m.LoginModule)},
  {
    path: 'verification',
    loadChildren: () => import('./views/verification/verification.module').then(m => m.VerificationModule)
  },
  {path: 'cabinet', loadChildren: () => import('./views/cabinet/cabinet.module').then(m => m.CabinetModule)},
  {path: '', redirectTo: 'dashboard', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
