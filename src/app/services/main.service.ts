import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import {pki} from 'node-forge'
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class MainService {

  private API_URL = environment.API_URL;
  constructor(private http: HttpClient, private cookieService: CookieService) {}

  public get<T>(url: string, headers: any = {}): Observable<T> {
    return this.http.get<T>(this.requestFullPath(url), {
      headers: new HttpHeaders({ ...this.getHeader, ...headers }),
    });
  }

  public post<T, K>(url: string, body: K, headers: any = {}): Observable<T> {
    return this.http.post<T>(this.requestFullPath(url), body, {
      headers: new HttpHeaders({ ...this.getHeader, ...headers }),
    });
  }

  public delete<T>(url: string, headers: any = {}): Observable<T> {
    return this.http.delete<T>(this.requestFullPath(url), {
      headers: new HttpHeaders({ ...this.getHeader, ...headers }),
    });
  }

  private requestFullPath(path: string): string {
    return this.API_URL + path;
  }

  get getHeader() {
    return {
      'Content-Type': 'application/json',
      'X-Lang': 'RUS',
      // 'X-Auth-Token': this.authToken,
    };
  }
}
