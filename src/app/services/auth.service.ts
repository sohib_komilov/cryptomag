import {Injectable} from '@angular/core';
import {MainService} from "./main.service";
import {Observable} from "rxjs";
import {cookieResponse, signCodeResponse, signInConfirm, username} from "../types/auth.types";
import {pki} from "node-forge";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: MainService) {
  }

  signInCodeSend(username: username): Observable<signCodeResponse> {
    return this.http.post<signCodeResponse, username>('auth/sign/init', username || '')
  }

  signInConfirm(data: signInConfirm): Observable<cookieResponse> {
    return this.http.post<cookieResponse, signInConfirm>('auth/sign/confirm', data || {})
  }

  signCheck(password: string, encKey: string, identity: string): Observable<signCodeResponse> {
    return this.http.post('auth/sign/check', {
      password: this.encWithPubKey(password, encKey),
      identity: identity,
      captcha: null,
      agreement: '1'
    } || {})
  }

  encWithPubKey(valueToEncrypt: string, pubKey: string): string {
    const realPubKey =
      '-----BEGIN PUBLIC KEY-----\n' + pubKey + '\n-----END PUBLIC KEY-----';
    const rsa = pki.publicKeyFromPem(realPubKey);
    return window.btoa(rsa.encrypt(valueToEncrypt.toString()));
  }
}
